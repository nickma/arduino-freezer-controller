#include <iostream>
#include <limits>
#include "greatest_1.4.2.h"
#include "../FreezerLogic.h"

using namespace std;

TEST cooling_test(void) {
    bool rc;

    FreezerSetup setup;
    FreezerSensors sensors;
    FreezerActors actors;

    //Equals threshold pump_on off
    sensors.temp = -17.0;
    actors.pump_on = false;
    rc = get_next_pump_state(sensors, actors, setup);
    ASSERT_EQ(false, rc);
    //Equals threshold pump_on on -> stays on
    sensors.temp = -17.0;
    actors.pump_on = true;
    rc = get_next_pump_state(sensors, actors, setup);
    ASSERT_EQ(true, rc);

    // Warmer than threshold -> true
    sensors.temp = -17.0 + COOL_HYSTERESIS_THRESHOLD;
    actors.pump_on = false;
    rc = get_next_pump_state(sensors, actors, setup);
    ASSERT_EQ(true, rc);
    sensors.temp = -17.0 + COOL_HYSTERESIS_THRESHOLD + 0.1;
    actors.pump_on = true;
    rc = get_next_pump_state(sensors, actors, setup);
    ASSERT_EQ(true, rc);

    // Colder than threshold, pump_on on -> turn off
    sensors.temp = -17.0 - COOL_HYSTERESIS_THRESHOLD - 0.1;
    actors.pump_on = true;
    rc = get_next_pump_state(sensors, actors, setup);
    ASSERT_EQ(false, rc);
    sensors.temp = -17.0 - COOL_HYSTERESIS_THRESHOLD;
    actors.pump_on = false;
    rc = get_next_pump_state(sensors, actors, setup);
    ASSERT_EQ(false, rc);
    //FAILm("(expected failure)");

    PASS();
}

TEST alarm_test() {
    FreezerSetup setup;
    FreezerSensors sensors;

    FreezerAlarm alarm_state;

    // tests here we go
    sensors.temp = -17.0;
    alarm_state = get_alarm_state(sensors, setup);
    ASSERT_EQ(NONE, alarm_state);

    sensors.temp = -17.0 + COOL_ALARM_THRESHOLD;
    alarm_state = get_alarm_state(sensors, setup);
    ASSERT_EQ(TOO_WARM, alarm_state);

    // pause alarm
    setup.alarm_paused = true;
    alarm_state = get_alarm_state(sensors, setup);
    ASSERT_EQ(TOO_WARM, alarm_state);
    PASS();
}


GREATEST_MAIN_DEFS();

int main(int argc, char **argv)
{
    //update_facts(state);
    
    GREATEST_MAIN_BEGIN();
    RUN_TEST(cooling_test);
    RUN_TEST(alarm_test);
    GREATEST_MAIN_END();        /* display results */
    return 0;
}