/*
 * Board setup: Arduino Nano, Atmega328P (old Bootloader)
 * Serial programmer: Arduino as ISP
 */

#include <Wire.h>
#include <DallasTemperature.h>

#define VERSION "0.3"

// include the library code:
#include <LiquidCrystal.h>
#include "FreezerLogic.h"
#include <EEPROM.h>

byte test_mode=0;
#define EEADDRESS_TEMP 0   //Location we want the data to be put.

/********************************************************************/
// button
// on mini only pins {2,3} are usable for interrupts - https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
const byte button_up_pin = 2, button_down_pin = 3, button_alarm_off_pin = 4;
// sensors
#define DOOR_PIN          6
#define FOOOOO_FREE_PIN   7
// LCD display
const byte rs = 8, en = 9, d4 = 10, d5 = 11, d6 = 12, d7 = 13;

// precison temp sensor MCP9808
// Arduino nano/mini: A4..SDA, A5..SCL
//  CircuitBoard Yellow..SDA..A4, Green..SCL..A5, Red..+5V, Blk..GND
#include "Adafruit_MCP9808.h"
Adafruit_MCP9808 inside_temp = Adafruit_MCP9808();

// actors
#define LIGHT_PIN         A1
#define COMPRESSOR_PIN    A2
#define BEEPER_PIN        A3

// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);


// button interrupt handling with debounce
volatile struct btn_s {
  int press_delta = 0;
  unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
  const unsigned long debounceDelay = 100;    // the debounce time; increase if the output flickers
} btn;
void add_delta(const int delta) {
  const unsigned long ms = millis();
  if ((ms - btn.lastDebounceTime) <= btn.debounceDelay)
    return;
  btn.lastDebounceTime = ms;
  // TODO move debounce logic to separate method
  btn.press_delta += delta;
}
void handle_button_up() {
  add_delta(+1);
}
void handle_button_down() {
  add_delta(-1);
}

void setup_io_and_sensors() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  
  Serial.println("going to Attaching ISR");
  delay(250);
  // button
  // on mini only pins {2,3} are usable for interrupts - https://www.arduino.cc/reference/en/language/functions/external-interrupts/attachinterrupt/
  pinMode(button_up_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button_up_pin), handle_button_up, FALLING);
  pinMode(button_down_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(button_down_pin), handle_button_down, FALLING);
  pinMode(button_alarm_off_pin, INPUT_PULLUP);
  pinMode(DOOR_PIN, INPUT_PULLUP);

  // actors
  pinMode(LIGHT_PIN, OUTPUT);
  pinMode(COMPRESSOR_PIN, OUTPUT);
  pinMode(BEEPER_PIN, OUTPUT);
  FreezerActors init_states;
  propagate_to_actors(init_states);

  // Temp Sensor
  Wire.begin(); // join i2c bus (address optional for master)
  // Make sure the sensor is found, you can also pass in a different i2c
  // address with tempsensor.begin(0x19) for example
  if (!inside_temp.begin()) {
      Serial.println("E: Couldn't find MCP9808!");
      
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("ERROR temp sensor");

      init_states.pump_on = true;
      init_states.beeper = true;
      propagate_to_actors(init_states);
      while(1);
  }
  // Mode Resolution SampleTime
  //  0    0.5°C       30 ms
  //  1    0.25°C      65 ms
  //  2    0.125°C     130 ms
  //  3    0.0625°C    250 ms
  inside_temp.setResolution(3); // sets the resolution mode of reading, the modes are defined in the table bellow:
  Serial.print("Inside_temp resolution:"); Serial.println(inside_temp.getResolution());

}


// the setup function runs once when you press reset or power the board
void setup() {
  Serial.begin(115200);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Hi   v" VERSION);
  
  
  setup_io_and_sensors();
}

/**
 * @return values changed or not
 */
bool handle_up_down_btns(FreezerSetup &setup) {
  if (!btn.press_delta)
    return false;
  setup.target_temp += btn.press_delta;
  btn.press_delta = 0;

  EEPROM.put(EEADDRESS_TEMP, setup.target_temp);
  Serial.print("Set target temp to:");
  Serial.println(setup.target_temp);
  return true;
}
void propagate_to_actors(const FreezerActors &actors) {
  digitalWrite(LIGHT_PIN,       actors.light);
  digitalWrite(COMPRESSOR_PIN,  !actors.pump_on);
  digitalWrite(BEEPER_PIN,      !actors.beeper);
}

void propagate_user_output(const FreezerSetup &setup, 
        const FreezerSensors &sensors, 
        const FreezerActors &actors) {
    // outputs
    Serial.print("Beeper:");
    Serial.print(actors.beeper);
    Serial.print(", paused:");
    Serial.println(setup.alarm_paused);

    // LCD
    lcd.clear();
    const uint8_t column=0;
    lcd.setCursor(column, 0);
    // print the number of seconds since reset:
    lcd.print(sensors.temp);
    lcd.print((char)223); //degree sign
    lcd.print("C Tgt:");
    lcd.print((int)setup.target_temp);

    //static bool toggle = false;
    //toggle = !toggle;
    
    lcd.setCursor(column, 1);
    lcd.print("Lid:");
    lcd.print(sensors.door_open_sec);
    lcd.print("s Pump:");
    lcd.print(actors.pump_on);
    lcd.print("s");
}

inline bool is_sensor_poll_time(const uint8_t secs_mod_ten) {
  return secs_mod_ten == 1;;
}

void poll_sensors(const uint8_t secs_mod_ten, FreezerSensors &sensors) {
    // pre temp poll stage, wake up temp sensor(s)
    const bool sensor_wakeup = secs_mod_ten == 0;
    if(sensor_wakeup) {
        // wake up MSP9808 - power consumption ~200uA
        inside_temp.wake();  //wake up and guard wait 250ms
    }
    
    // temp poll stage
    if(is_sensor_poll_time(secs_mod_ten)) {
        sensors.temp = inside_temp.readTempC();
        inside_temp.shutdown(); // shutdown MSP9808 - power consumption ~0.1 mikro Ampere

        Serial.print("Inside Temp: "); Serial.print(sensors.temp); Serial.println("*C\t");
    }

    // Door sensor logic
    const bool door_open = digitalRead(DOOR_PIN) == HIGH;
    if (door_open) {
        sensors.door_open_sec++;
    } else {
        sensors.door_open_sec = 0;
    }
}

// the loop function runs over and over again forever
void loop() {
  static FreezerSetup    setup;
  static FreezerSensors  sensors;
  static FreezerActors   actors;
  static bool oneTimeSetup = true;

  if (oneTimeSetup) {
    oneTimeSetup = false;
    EEPROM.get(EEADDRESS_TEMP, setup.target_temp);
    if(isnan(setup.target_temp))
      setup.target_temp = -17;
  }

  // more frequent than every second because that would feel laggy
  const bool btn_pressed = handle_up_down_btns(setup);

  // second changed logic
  static unsigned long lastEx = 42;
  const unsigned long secs = millis()/1000;
  const bool second_change = lastEx != secs;
  if(second_change) {
    lastEx = secs;
  }

  if(second_change) {
    const uint8_t secs_mod_ten = secs % 10;
    poll_sensors(secs_mod_ten, sensors);

    if(is_sensor_poll_time(secs_mod_ten)) {
        // Pump actor logic
        const bool next_pump_state = get_next_pump_state(sensors, actors, setup);
        actors.pump_on = next_pump_state;

        Serial.print("next_pump_state:"); Serial.println(next_pump_state);
    }
    // door actor logic
    actors.light = (sensors.door_open_sec > 0);

    // Alarm actor logic
    FreezerAlarm alarm_state = get_alarm_state(sensors, setup);
    
    const bool pause_alarm_pressed = digitalRead(button_alarm_off_pin) == LOW;
    set_alarm_paused(pause_alarm_pressed, alarm_state, setup);
    actors.beeper = get_beeper_state(alarm_state, setup);
    // END of next state setup

    propagate_to_actors(actors);
  }

  if (second_change || btn_pressed) {
      // single point of user output
      propagate_user_output(setup, sensors, actors);
  }
}
