//
// Created by nickma on 22.10.18.
//

#ifndef LIEBHERR_FREEZER_FREEZER_LOGIC_H
#define LIEBHERR_FREEZER_FREEZER_LOGIC_H

#include <stdint.h>

#define COOL_HYSTERESIS_THRESHOLD   1.0
#define COOL_ALARM_THRESHOLD        3   //
#define DOOR_OPEN_ALARM_TIME        20

enum FreezerAlarm {
    NONE = 0,
    DOOR_OPEN,
    TOO_WARM,
};

struct FreezerSetup {
    float target_temp = -17.0;
    bool  alarm_paused = false;
};

struct FreezerSensors {
    float       temp = 0.0;
    uint16_t    door_open_sec = 0;
};

struct FreezerActors {
    bool    pump_on = false;
    bool    light = false;
    bool    beeper = false;
};


/**
Example usage:
  bool pause_alarm_pressed = digitalRead(button_alarm_off_pin) == LOW;
  FreezerAlarm alarm_state = get_alarm_state(sensors, setup);
  set_alarm_paused(pause_alarm_pressed, alarm_state, setup);
  actors.beeper = get_beeper_state(alarm_state, setup);
*/
bool get_next_pump_state(const FreezerSensors &sensors, const FreezerActors &actors, const FreezerSetup &setup);

FreezerAlarm get_alarm_state(const FreezerSensors &sensors, const FreezerSetup &setup);

void set_alarm_paused(const bool pause_alarm_pressed, const FreezerAlarm state, FreezerSetup &setup);

bool get_beeper_state(const FreezerAlarm alarm_state, const FreezerSetup &setup);

#endif //LIEBHERR_FREEZER_FREEZER_LOGIC_H
