//
// Created by nickma on 22.10.18.
//
#include "FreezerLogic.h"

bool get_next_pump_state(const FreezerSensors &sensors, const FreezerActors &actors, const FreezerSetup &setup) {
    auto offset_temp = sensors.temp;
    offset_temp += actors.pump_on ? COOL_HYSTERESIS_THRESHOLD : -COOL_HYSTERESIS_THRESHOLD;

    return offset_temp >= setup.target_temp;
}

FreezerAlarm get_alarm_state(const FreezerSensors &sensors, const FreezerSetup &setup) {
    if(sensors.temp - COOL_ALARM_THRESHOLD >= setup.target_temp)
        return TOO_WARM;
    if(sensors.door_open_sec >= DOOR_OPEN_ALARM_TIME) {
        return DOOR_OPEN;
    }

    return NONE;
}

// TODO make it a getter
void set_alarm_paused(const bool pause_alarm_pressed, const FreezerAlarm state, FreezerSetup &setup) {
    if (state == NONE) {
        setup.alarm_paused = false;
    } else {
        setup.alarm_paused |= pause_alarm_pressed;
    }
}

bool get_beeper_state(const FreezerAlarm state, const FreezerSetup &setup) {
    if (state == NONE) {
        return false;
    } else if (setup.alarm_paused) {
        return false;
    }

    return true;
}
